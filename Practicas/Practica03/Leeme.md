# Acceso a Datos con ADO .Net y Linq

Para esta practica usar el proyecto PatronRepositorio de la Carpeta AccesoADatos 

Debe crear los repositorios para las entidades:
+ Artista (Tabla Artist)
+ Cancion (Tabla Track)

Crear una Clase llamada TiendaMusica que debe ofrecer la siguiente funcionalidad.

1. Permitir la Creacion de un Artista.
2. Permitir Crear un Album para lo cual es necesario lo siguiente:
	+ El album debe pertenecer a un artista ya registrado
	+ El album debe tener al menos una canci�n 
3. Ofrecer tres propiedades para poder hacer consultas Linq para:
	+ Artistas
	+ Albums
	+ Canciones
