﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CarritoCompras
{
    internal abstract class Entrada
    {
        protected Dictionary<DayOfWeek, decimal> precios;
        protected List<DateTime> Feriados;

        protected Entrada()
        {
            LeerFeriados();
            LeerPrecios();
        }

        public decimal Precio(DateTime horario)
        {
            return EsFeriado(horario) ? precios[DayOfWeek.Sunday] : precios[horario.DayOfWeek];
        }

        private bool EsFeriado(DateTime horario)
        {
            return Feriados.Any(dia=> horario>dia && horario<= dia.AddDays(1).Date);
        }
        protected abstract void LeerPrecios();

        protected void LeerFeriados()
        {
            Feriados = new List<DateTime>();
            Feriados.Add(new DateTime(2016,07,28));
            Feriados.Add(new DateTime(2016, 07, 29));
            Feriados.Add(new DateTime(2016, 05, 1));
            Feriados.Add(new DateTime(2016, 08, 30));
        }
    }
}