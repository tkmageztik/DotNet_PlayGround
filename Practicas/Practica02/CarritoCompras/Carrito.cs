﻿using System;
using System.Collections.Generic;

namespace CarritoCompras
{
    class Carrito
    {
        private List<EntradaComprada> entradas;
        public Carrito(string pelicula, DateTime horario)
        {
            entradas = new List<EntradaComprada>();
            Pelicula = pelicula;
            Horario = horario;
        }

        public DateTime Horario { get; private set; }

        public string Pelicula { get; private set; }

        public void AgregarEntrada(Entrada entradaComprada, int cantidad)
        {
            entradas.Add(new EntradaComprada(entradaComprada, cantidad, this));
        }

        public decimal TotalAPagar()
        {
            decimal total = 0;
            entradas.ForEach(entrada => total += entrada.Precio());
            return total;

        }

        public void Pagar(IMedioPago medioPago)
        {
            //throw new NotImplementedException();
        }

        class EntradaComprada
        {
            public EntradaComprada(Entrada entradaComprada, int cantidad, Carrito funcion)
            {
                this.TipoEntradaComprada = entradaComprada;
                this.Cantidad = cantidad;
                this.Funcion = funcion;
            }

            public Carrito Funcion { get; private set; }

            public int Cantidad { get; private set; }
            public Entrada TipoEntradaComprada { get; private set; }

            public decimal Precio()
            {
                return TipoEntradaComprada.Precio(Funcion.Horario) * Cantidad;
            }

        }
    }

}