﻿using System;
using System.Collections.Generic;

namespace CarritoCompras
{
    internal class EntradaAdultoMayor : Entrada
    {

        protected override void LeerPrecios()
        {
            precios = new Dictionary<DayOfWeek, decimal>();
            precios.Add(DayOfWeek.Sunday, 18);
            precios.Add(DayOfWeek.Monday, 14);
            precios.Add(DayOfWeek.Tuesday, 10);
            precios.Add(DayOfWeek.Wednesday, 14);
            precios.Add(DayOfWeek.Thursday, 14);
            precios.Add(DayOfWeek.Friday, 14);
            precios.Add(DayOfWeek.Saturday, 18);
        }
    }
}