﻿using System;
using System.Collections.Generic;

namespace CarritoCompras
{
    internal class EntradaNiño : Entrada
    {

        protected override void LeerPrecios()
        {
            precios = new Dictionary<DayOfWeek, decimal>();
            precios.Add(DayOfWeek.Sunday, 15);
            precios.Add(DayOfWeek.Monday, 12);
            precios.Add(DayOfWeek.Tuesday, 5);
            precios.Add(DayOfWeek.Wednesday, 12);
            precios.Add(DayOfWeek.Thursday, 12);
            precios.Add(DayOfWeek.Friday, 12);
            precios.Add(DayOfWeek.Saturday, 15);
        }

    }
}