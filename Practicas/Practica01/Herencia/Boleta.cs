﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Boleta : Comprobante
    {
        public Boleta(DateTime emitidaEn,string razoSocial)
        {
            this.EmitidoEn = emitidaEn;
            this.RazonSocial = razoSocial;
            GeneraNumeroDeSerie();
        }
        protected override void CalculaTotales()
        {
            Total = 0;
            Detalles.ForEach(x => { Total += x.Monto; });
            this.SubTotal = Total;
            this.Impuestos = 0m;
        }

        protected override void GeneraNumeroDeSerie()
        {
            this.Serie = 1;
            this.Numero = 10;
        }

        public override string Imprimir()
        {
            StringBuilder impresion = new StringBuilder();

            impresion.AppendLine($"Boleta de Venta {NumeroDocumento}");
            impresion.AppendLine($"Emision: {EmitidoEn.ToString("dd-MM-yyyy")}");
            impresion.AppendLine($"Razón Social: {RazonSocial}");
            impresion.AppendLine("=======================Detalle================================");
            Detalles.ForEach(x =>
            {
                impresion.AppendLine($"{x.Descripcion}                  {x.Monto}");
            });
            impresion.AppendLine("==============================================================");
            impresion.AppendLine($"Total\t{Total}");
            return impresion.ToString();
        }
    }
}
