﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Factura factura = new Factura(DateTime.Now, DateTime.Now.AddDays(30), "21234567891", "ACME SAC");
            factura.AgregarDetalle(new DetalleComprobante {Descripcion = "item1", Monto=100});
            Console.WriteLine(factura.Imprimir());

            Boleta boleta = new Boleta(DateTime.Now,  "ACME SAC");
            boleta.AgregarDetalle(new DetalleComprobante { Descripcion = "Por Consumo", Monto = 100 });
            Console.WriteLine(boleta.Imprimir());

            ReciboPorHonorarios recibo = new ReciboPorHonorarios(DateTime.Now, "21234567891", "ACME SAC");
            recibo.AgregarDetalle(new DetalleComprobante { Descripcion = "Servicios prestados", Monto = 100 });
            Console.WriteLine(recibo.Imprimir());

            Console.Read();
        }
    }
}
