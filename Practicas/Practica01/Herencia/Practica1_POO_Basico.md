# Practica

Desarrollar una aplicación para registro de ventas
Funcionalidad principal, permitir registrar un comprobante de pago

Comprobantes de pago aceptados por ahora
1. Factura  (Impuesto IGV 18%)
2. Boleta   (Incluido en el total)
3. Recibo por honorarios (Impuesto Renta de 4ta Categoria 10%)

Datos que se deben almacenar:

1. Nro del documento
2. Fecha Emision
3. Fecha de Vencimiento
4. Ruc
5. RazonSocial
6. Detalles
7. Sub Total  (Calculado)
8. Impuestos  (Calculado)
9. Total      (Calculado)

Una vez ingresados los datos de la factura se debe imprimir la misma
(En Consola)