﻿using System;
using System.Text;

namespace Herencia
{
    class Factura : Comprobante
    {

        public Factura(DateTime emitidaEn, DateTime venceEn, string ruc, string razonSocial )
        {
            EmitidoEn = emitidaEn;
            VenceEl = venceEn;
            Ruc = ruc;
            RazonSocial = razonSocial;
            GeneraNumeroDeSerie();
        }

        public DateTime VenceEl { get; private set; }

        
        protected override void CalculaTotales()
        {
            Total = 0;
            Detalles.ForEach(x=> { Total += x.Monto; });
            this.SubTotal = decimal.Round((Total/1.18m), 2);
            this.Impuestos = Total - SubTotal;
        }

        protected override void GeneraNumeroDeSerie()
        {
            this.Serie = 1;
            this.Numero = 10;
        }

        public override string Imprimir()
        {
            StringBuilder impresion = new StringBuilder();

            impresion.AppendLine($"Factura {NumeroDocumento}");
            impresion.AppendLine($"Emision: {EmitidoEn.ToString("dd-MM-yyyy")}  Vencimiento: {VenceEl.ToString("dd-MM-yyyy")}");
            impresion.AppendLine($"RUC: {Ruc}  Razón Social: {RazonSocial}" );
            impresion.AppendLine("=======================Detalle================================" );

            Detalles.ForEach(x =>
            {
                impresion.AppendLine($"{x.Descripcion}                  {x.Monto}");
            });
            impresion.AppendLine("==============================================================");
            impresion.AppendLine($"SubTotal\t{SubTotal}");
            impresion.AppendLine($"IGV (18%)\t{Impuestos}");
            impresion.AppendLine($"SubTotal\t{Total}");
            return impresion.ToString();
        }
    }
}
