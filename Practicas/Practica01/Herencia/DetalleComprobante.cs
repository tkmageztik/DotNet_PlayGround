﻿namespace Herencia
{
    public class DetalleComprobante
    {
        public decimal Monto { get; set; }
        public string Descripcion { get; set; }
    }
}