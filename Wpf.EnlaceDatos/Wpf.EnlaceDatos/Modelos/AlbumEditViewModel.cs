﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.EnlaceDatos.Modelos
{
    class AlbumEditViewModel : INotifyPropertyChanged
    {
        public int AlbumId { get; set; }
        public int ArtistaId { get; set; }

        private string _artistaNombre;
        private string _albumTitulo;
        public string ArtistaNombre
        {
            get { return this._artistaNombre; }
            set
            {
                if (this._artistaNombre != value)
                {
                    _artistaNombre = value;
                    this.NotiFyPropertyChanged("ArtistaNombre");
                }
            }
        }

        private void NotiFyPropertyChanged(string propiedad)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propiedad));
        }

        public string AlbumTitulo
        {
            get { return _albumTitulo; }
            set
            {
                if (this._albumTitulo != value)
                {
                    _albumTitulo = value;
                    this.NotiFyPropertyChanged("AlbumTitulo");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
