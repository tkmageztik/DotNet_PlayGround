﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.EnlaceDatos.Modelos;
using Wpf.EnlaceDatos.Servicios;

namespace Wpf.EnlaceDatos
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AlbumServicio servicio;
        private AlbumEditViewModel modelo = null;
        public MainWindow()
        {
            InitializeComponent();
            servicio = new AlbumServicio();
            this.DataContext = modelo;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            modelo = servicio.TraerAlbum(Convert.ToInt32(ArtistaId.Text));
            this.DataContext = modelo;
        }
    }
}
