﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf.EnlaceDatos.Dominio;
using Wpf.EnlaceDatos.Modelos;

namespace Wpf.EnlaceDatos.Servicios
{
    class AlbumServicio
    {
        private readonly Chinook baseDatos;

        public AlbumServicio()
        {
            baseDatos = new Chinook();
        }

        public AlbumEditViewModel TraerAlbum(int id)
        {
            var album = baseDatos.Album.Include("Artist").FirstOrDefault(x=>x.AlbumId==id);
            return new AlbumEditViewModel
            {
                AlbumId = album.AlbumId,
                ArtistaId = album.ArtistId,
                AlbumTitulo = album.Title,
                ArtistaNombre = album.Artist.Name
            };
        }
    }
}
