﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro
{
    class Program
    {
        static void Main(string[] args)
        {
            EjemploConexiones();
            EjemploComandos();
        }

        private static void EjemploComandos()
        {
            Console.WriteLine("Ejemplo de Comandos");
            Comandos.LeerInformacion();
            Comandos.ActualizarInformacion();
            Console.Write("Presione una tecla para continuar..");
            Console.Read();

        }

        private static void EjemploConexiones()
        {
            Console.WriteLine("Ejemplo de Conexiones");
            Conexiones.EjemploConexion();
            Conexiones.EjemploConexionRecomendado();
            Console.Write("Presione una tecla para continuar..");
            Console.Read();
        }

    }
}
