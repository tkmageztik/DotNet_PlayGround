﻿using Cliente.Dominio;
using System.Data.Entity;

namespace Cliente.Datos
{
    public class MatriculasContexto : DbContext
    {
        public IDbSet<Estudiantes> Estudiantes {get;set;}
        public IDbSet<Cursos> Cursos {get; set;}
        public MatriculasContexto() : base("name=Matriculas")
        {

        }
    }
}
