﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cliente.Dominio
{
    public class Cursos : Entidad
    {
        public Cursos()
        {
            this.Estudiantes = new HashSet<Estudiantes>();
        }
        public string Nombre { get; set; }
        public int Semestre { get; set; }

        public virtual ICollection<Estudiantes> Estudiantes { get; set; }
    }
}
