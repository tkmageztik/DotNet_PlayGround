﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cliente.Dominio
{
    public class Estudiantes : Entidad
    {
        public Estudiantes()
        {
            this.Cursos = new HashSet<Cursos>();
        }
        public string Nombre { get; set; }
        public string Direccion { get; set; }

        public virtual ICollection<Cursos> Cursos { get; set; }
    }
}
