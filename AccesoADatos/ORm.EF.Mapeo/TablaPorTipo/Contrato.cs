﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ORm.EF.Mapeo.TablaPorTipo.Modelo
{
    public abstract class Entidad
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)] // solo para SQL server
        public int Id { get; set; }

    }

    public abstract class Contrato : Entidad
    {
        public Contrato()
        {
            this.Clientes = new HashSet<Cliente>();
        }
        [DataType(DataType.Date)]
        [Required]
        public DateTime Inicio { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime Finaliza { get; set; }

        public virtual ICollection<Cliente> Clientes { get; set; }
    }


    //[Table("ContratoCredito")] para mapeo Tabla por Tipo usando atributos
    public class ContratoCredito : Contrato
    {
        [Required]
        public float Limite { get; set; }

        [Column(TypeName = "char")]
        [StringLength(3)]
        [Required]
        public string Moneda { get; set; }

        [Required]
        public float LineaUtilizada { get; set; }
    }

    //[Table("ContratoPrepago")] para mapeo Tabla por Tipo usando atributos
    public class ContratoPrepago : Contrato
    {
        [Required]
        public float MontoPrepago { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        [Required]
        public string PeriodoPrepago { get; set; }
    }
}
