namespace ORm.EF.Mapeo.Migrations.ContextoTablaPorTipoConcreto
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContratoCredito",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Inicio = c.DateTime(nullable: false),
                        Finaliza = c.DateTime(nullable: false),
                        Limite = c.Single(nullable: false),
                        Moneda = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContratoPrepago",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Inicio = c.DateTime(nullable: false),
                        Finaliza = c.DateTime(nullable: false),
                        MontoPrepago = c.Single(nullable: false),
                        PeriodoPrepago = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ContratoPrepago");
            DropTable("dbo.ContratoCredito");
        }
    }
}
