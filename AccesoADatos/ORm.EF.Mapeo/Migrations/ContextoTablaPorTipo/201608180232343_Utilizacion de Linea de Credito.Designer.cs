// <auto-generated />
namespace ORm.EF.Mapeo.Migrations.ContextoTablaPorTipo
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UtilizaciondeLineadeCredito : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UtilizaciondeLineadeCredito));
        
        string IMigrationMetadata.Id
        {
            get { return "201608180232343_Utilizacion de Linea de Credito"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
