namespace ORm.EF.Mapeo.Migrations.ContextoTablaPorTipo
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UtilizaciondeLineadeCredito : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContratoCredito", "LineaUtilizada", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContratoCredito", "LineaUtilizada");
        }
    }
}
