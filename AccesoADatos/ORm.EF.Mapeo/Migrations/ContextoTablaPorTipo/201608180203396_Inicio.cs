namespace ORm.EF.Mapeo.Migrations.ContextoTablaPorTipo
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contratoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Inicio = c.DateTime(nullable: false),
                        Finaliza = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContratoCredito",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Limite = c.Single(nullable: false),
                        Moneda = c.String(nullable: false, maxLength: 3, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contratoes", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.ContratoPrepago",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        MontoPrepago = c.Single(nullable: false),
                        PeriodoPrepago = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contratoes", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContratoPrepago", "Id", "dbo.Contratoes");
            DropForeignKey("dbo.ContratoCredito", "Id", "dbo.Contratoes");
            DropIndex("dbo.ContratoPrepago", new[] { "Id" });
            DropIndex("dbo.ContratoCredito", new[] { "Id" });
            DropTable("dbo.ContratoPrepago");
            DropTable("dbo.ContratoCredito");
            DropTable("dbo.Contratoes");
        }
    }
}
