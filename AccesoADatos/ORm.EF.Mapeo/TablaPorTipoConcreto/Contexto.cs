﻿using ORm.EF.Mapeo.TablaPorTipoConcreto.Modelo;
using System.Data.Entity;

namespace ORm.EF.Mapeo.TablaPorTipoConcreto
{
    class ContextoTablaPorTipoConcreto : DbContext
    {
        // creo un DbSet para mi Jerarquia
        public DbSet<Contrato> Contratos { get; set; }
        public ContextoTablaPorTipoConcreto() 
            : base("name=PorTipoConcreto")
        {

        }

        /// <summary>
        /// aqui defino como se mapeara mi jerarqui en la bd
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // mapeo por tipo concreto
            modelBuilder.Entity<ContratoCredito>()
                .Map(m =>
                {
                    m.MapInheritedProperties();
                    m.ToTable("ContratoCredito");
                }
                );

            modelBuilder.Entity<ContratoPrepago>()
                .Map(m=>
                {
                    m.MapInheritedProperties();
                    m.ToTable("ContratoPrepago");
                }
                );
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
