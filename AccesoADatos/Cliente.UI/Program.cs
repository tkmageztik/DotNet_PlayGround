﻿using Cliente.Datos;
using Cliente.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cliente.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Cursos javaScript = new Cursos
            {
                Nombre = "java Script",
                Semestre = 2,
                Estudiantes = new List<Estudiantes>()
                {
                    new Estudiantes
                    {
                        Nombre="Pedro Cutipa",
                        Direccion = "La Aurora 12547"
                    }
                }
            };
            using (var bd = new MatriculasContexto())
            {

                bd.Cursos.Add(javaScript);
                bd.SaveChanges();
            }
        }

        private static void CreandoAlumnos()
        {
            // matriculando a Juan Perez

            Estudiantes juan = new Estudiantes
            {
                Nombre = "Juan Perez",
                Direccion = "Cesar vallejo 123"
             
            };

            using (var bd = new MatriculasContexto())
            {
                juan.Cursos.Add(bd.Cursos.Single(x => x.Id == 1));

                bd.Estudiantes.Add(juan);
                bd.SaveChanges();
            }
        }

        private static void CreandoObjetosNuevos()
        {
            // matriculando a Juan Perez

            Estudiantes juan = new Estudiantes
            {
                Nombre = "Juan Perez",
                Direccion = "Cesar vallejo 123",
                Cursos = new List<Cursos>()
                {
                    new Cursos {
                        Nombre = "Programacion en .Net",
                        Semestre =1
                    }
                }
            };

            using (var bd = new MatriculasContexto())
            {
                bd.Estudiantes.Add(juan);
                bd.SaveChanges();
            }
        }
    }
}
