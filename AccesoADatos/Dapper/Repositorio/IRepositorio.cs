﻿using DapperExtensions;
using System.Collections.Generic;

namespace DapperMicro.Repositorio
{
    public interface IRepositorio<T> where T : class
    {
        int Agregar(T entity);
        bool Actualizar(T entity);
        bool Eliminar(T entity);
        T GetById(int id);
        IEnumerable<T> Listar();
        IEnumerable<T> ListarPorConsulta(string consulta);

        IEnumerable<T> FiltroPorUnCampo(IPredicate predicado);

        IEnumerable<T> FiltroVariosCampos(IPredicateGroup predicados);

        IEnumerable<T> ObtenerProcedimiento(string nombreProcedimiento, object variables);
    }
}
