﻿using Dapper;
using DapperExtensions;
using DapperMicro.Modelo;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DapperMicro.Repositorio
{
    public class RepositorioCustomer
    {
        private string cadenaConexion;
        public RepositorioCustomer()
        {
            cadenaConexion = "Data Source=(local)\\SQLExpress; Database=Chinook; Integrated Security=True";
        }
        public bool Actualizar(Customer entity)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Update(entity);
                conexionSql.Close();
                return result;
            }
        }

        public int Agregar(Customer entity)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Insert(entity);
                conexionSql.Close();
                return result;
            }
        }

        public Customer GetById(int id)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Get<Customer>(id);
                conexionSql.Close();
                return result;
            }
        }

        public bool Eliminar(Customer entity)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Delete(entity);
                conexionSql.Close();
                return result;
            }
        }

        public IEnumerable<Customer> Listar()
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.GetList<Customer>();
                conexionSql.Close();
                return result;
            }
        }

        public IEnumerable<Customer> ListarPorConsulta(string consulta)
        {            
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Query<Customer>(consulta);
                conexionSql.Close();
                return result;
            }
        }
    }
}
