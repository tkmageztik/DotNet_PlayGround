﻿using Dapper;
using DapperExtensions;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;
using System.Data;

namespace DapperMicro.Repositorio
{
    public class RepositorioBase<T> : IRepositorio<T> where T : class
    {
        private string cadenaConexion;
        public RepositorioBase()
        {
            cadenaConexion = "Data Source=(local)\\sqlexpress; Database=Chinook; Integrated Security=True";
        }
        public bool Actualizar(T entity)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Update(entity);
                conexionSql.Close();
                return result;
            }
        }

        public int Agregar(T entity)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Insert(entity);
                conexionSql.Close();
                return result;
            }
        }

        public T GetById(int id)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Get<T>(id);
                conexionSql.Close();
                return result;
            }
        }

        public bool Eliminar(T entity)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Delete(entity);
                conexionSql.Close();
                return result;
            }
        }

        public IEnumerable<T> Listar()
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.GetList<T>();
                conexionSql.Close();
                return result;
            }
        }

        public IEnumerable<T> ListarPorConsulta(string consulta)
        {            
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.Query<T>(consulta);
                conexionSql.Close();
                return result;
            }
        }

        public IEnumerable<T> FiltroPorUnCampo(IPredicate predicado)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.GetList<T>(predicado);
                conexionSql.Close();
                return result;
            }
        }

        public IEnumerable<T> FiltroVariosCampos(IPredicateGroup predicados)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();
                var result = conexionSql.GetList<T>(predicados);
                conexionSql.Close();
                return result;
            }
        }

        public IEnumerable<T> ObtenerProcedimiento(string nombreProcedimiento, object variables)
        {
            using (var conexionSql = new SqlConnection(cadenaConexion))
            {
                conexionSql.Open();                
                var result = conexionSql.Query<T>(nombreProcedimiento, variables, commandType: CommandType.StoredProcedure);
                conexionSql.Close();
                return result;
            }
        }
    }
}
