CREATE PROCEDURE dbo.TrackConRelaciones
AS
BEGIN
	SELECT track.[TrackId]
		  ,track.[Name]
		  ,alb.[Title]
		  ,media.[Name]
		  ,gen.[Name]
		  ,track.[Composer]
		  ,track.[Milliseconds]
		  ,track.[Bytes]
		  ,track.[UnitPrice]
	  FROM dbo.Track track
	  INNER JOIN dbo.Genre gen ON gen.GenreId=track.GenreId
	  INNER JOIN dbo.Album alb ON alb.AlbumId=track.AlbumId 
	  INNER JOIN dbo.MediaType media ON media.MediaTypeId=track.MediaTypeId
END
GO
