CREATE PROCEDURE dbo.ObtenerCustomerPorNombre
	@firstName nvarchar(40)
AS
BEGIN
	SELECT TOP 10 [CustomerId]
      ,[FirstName]
      ,[LastName]
      ,[Company]
      ,[Address]
      ,[City]
      ,[State]
      ,[Country]
      ,[PostalCode]
      ,[Phone]
      ,[Fax]
      ,[Email]
      ,[SupportRepId]	 
	FROM dbo.Customer
	WHERE FirstName LIKE '%'+ @firstName +'%'
END
GO
