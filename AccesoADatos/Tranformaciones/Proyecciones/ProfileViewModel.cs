﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tranformaciones.Proyecciones
{
    class PersonViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public AddressViewModel Address { get; set; }

        public List<NotesViewModel> Notes { get; set; }
    }



    class AddressViewModel
    {
        public string PersonaDireccionLineaUno { get; set; }
        public string PersonaPaisResidencia { get; set; }

    }

    class NotesViewModel
    {
        public string Nota { get; set; }
    }


}
