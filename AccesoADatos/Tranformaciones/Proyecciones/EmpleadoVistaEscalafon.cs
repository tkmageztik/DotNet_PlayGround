﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tranformaciones.Proyecciones
{
    public class EmpleadoVistaEscalafon
    {
        public string Name { get; set; }
        public int YearsInTheCompany { get; set; }

        public int Edad { get; set; }

        public DateTime FechaContrato { get; set; }
    }
}
