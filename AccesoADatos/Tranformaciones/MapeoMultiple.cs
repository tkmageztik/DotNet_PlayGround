﻿using MVC5Course.Automapper.Domain.ProfilesDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tranformaciones.Proyecciones;

namespace Tranformaciones
{
    class MapeoMultiple
    {
        public static void Main (string[] args)
        {
            AutoMapper.Mapper.CreateMap<Person, PersonViewModel>()
                .ForMember(
                        destino => destino.FullName,
                        options => options.MapFrom(
                                    origen => $"{origen.Firstname}, {origen.Lastname}"));


            AutoMapper.Mapper.CreateMap<Address, AddressViewModel>()
                .ForMember(dest =>
                            dest.PersonaDireccionLineaUno,
                            opt => opt.MapFrom(orig => orig.FirstLine))
                .ForMember(dest => dest.PersonaPaisResidencia,
                opt => opt.MapFrom(orig => orig.Country));

            AutoMapper.Mapper.CreateMap<Note, NotesViewModel>()
                .ForMember(dest => dest.Nota,
                opt => opt.MapFrom(orig =>
                     $"{orig.Id} - {orig.Text}"
                ));


            var persona = new Person
            {
                Address = new Address { Country = "Peru",FirstLine="Marconi 1234", PostCode="00051" },
                Firstname = "Juan",
                Lastname = "Perez",
                Id = 1,
                Notes = new List<Note>
                {
                    new Note {Id=1, Text="Cliente Con Deuda vencida" },
                    new Note {Id=2, Text="Avisar cuando venga" },
                }
            };

            var lista = new List<Person>
            {
                persona,
                persona
            };
            var personaVista = AutoMapper.Mapper.Map<Person, PersonViewModel>(persona);

            var listaPersonas = AutoMapper.Mapper.Map<List<Person>, List<PersonViewModel>>(lista);

            Console.WriteLine($"{personaVista.FullName}");

        }
    }
}
