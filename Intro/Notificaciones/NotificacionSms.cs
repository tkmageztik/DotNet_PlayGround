﻿using System;
using System.Collections.Generic;

namespace Notificaciones
{
    class NotificacionSms :INotificacion
    {
        public NotificacionSms()
        {
            destinatarios = new List<string>();
        }
        private List<string> destinatarios;
        public string Mensaje { get; internal set; }

        public void AgregarDestinatario(string destinatario)
        {
            destinatarios.Add(destinatario);
        }

        public void Enviar()
        {
            Console.WriteLine($"Enviando SMS ");
            destinatarios.ForEach(i => { Console.WriteLine($"Enviando a {i}"); });
            Console.WriteLine($"mensaje: {Mensaje}");
        }
    }
}