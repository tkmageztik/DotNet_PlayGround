﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Cuadrilatero cuadrilatero = new Cuadrilatero(new Punto(2, 10), 5, 20);
            Circulo circulo = new Circulo(new Punto(15, 50), 50);
            Figura figura = cuadrilatero as Figura;

            circulo.AgregarEtiqueta("un circulo azul");
            DibujarFigura(cuadrilatero);
            DibujarFigura(circulo);
            DibujarFigura(figura);

            Console.Read();
        }

        static void DibujarFigura(Figura figuraADibujar)
        {
            figuraADibujar.Dibujar();
            figuraADibujar.MostrarEtiquetas(new Punto(1, 2));
        }
    }
}
