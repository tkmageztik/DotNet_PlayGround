﻿# Modificadores de Acceso

permiten definir la accesibilidad de clases, metodos, attributos
## Public 
Hace que el componente sea público, es decir accessible fuera del alcance

    public class Planilla
    {
        public TipoPlanilla Tipo {get; set;}
    }

## Private
Hace que el componente sea accesible desde dentro del alcance, contenedor

    public class Planilla
    {
        private List<FilaPlanilla> Filas;
        public TipoPlanilla Tipo {get; set;}

        public void Procesar()
        {
            Filas.Add(new FilaPlanilla());
        }
    }

## Internal
Hace visible público el elemento solo dentro del Assembly

    public class Planilla
    {
        private List<FilaPlanilla> Filas;
        public TipoPlanilla Tipo {get; set;}

        public void Procesar()
        {
            Filas.Add(new FilaPlanilla());
        }

        internal List<FilaPlanilla> Detalle()
        {
            return Filas;
        }
    }


