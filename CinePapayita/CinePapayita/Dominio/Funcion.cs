﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinePapayita.Dominio
{
    public class Funcion : Entidad
    {
        public Funcion()
        {
            EntradasDisponibles = new HashSet<Entrada>();
        }
        public DateTime Horario { get; set; }
        public virtual ICollection<Entrada> EntradasDisponibles { get; set; }

        public int PeliculaId { get; set; }

        public int SalaId { get; set; }

        public virtual Pelicula Pelicula {get ; set;}

        public virtual Sala Sala { get; set; }
    }
}
