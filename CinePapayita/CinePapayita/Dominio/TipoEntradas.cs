﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinePapayita.Dominio
{
    public abstract class Entrada : Entidad
    {
        public Entrada()
        {
            Precios = new HashSet<Precio>();
        }
        public virtual ICollection<Precio> Precios { get; set; }

        public virtual ICollection<Funcion> Funciones { get; set; }

        public bool EsVigente { get; set; }

        internal decimal ObtienePrecio(Funcion funcion, List<Feriados> feriados)
        {
            DayOfWeek dia;
            if (Esferiado(feriados, funcion.Horario))
                dia = DayOfWeek.Sunday;
            else
                dia = funcion.Horario.DayOfWeek;

            var precio = Precios.First(p => p.Dia == dia);

            //validar si es nulo y preguntar al cliente que hacer en ese caso
            return precio.Valor;
        }

        private bool Esferiado(List<Feriados> feriados, DateTime horario)
        {
            return feriados.Any(x => x.Fecha == horario);
        }
    }

    public class MenorDeEdad : Entrada
    {

    }

    public class Adulto : Entrada
    {

    }

    public class AdultoMayor : Entrada
    {

    }


}
