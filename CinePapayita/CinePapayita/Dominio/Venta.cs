﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinePapayita.Dominio
{
    public class Venta : Entidad
    {
        public Venta()
        {
            Tickets = new HashSet<VentaDetalle>();
        }

        public Venta(Funcion funcion, string nombreCliente)
        {
            Funcion = funcion;
            NombreCliente = nombreCliente;
        }

        public string NombreCliente { get; set; }
        public int FuncionId { get; set; }

        public virtual Funcion Funcion { get; set; }

        public virtual ICollection<VentaDetalle> Tickets { get; set; }

        internal void AgregaTicket(Entrada entrada, int cantidad, List<Feriados> feriados)
        {
            var ticket = new VentaDetalle(entrada, cantidad);
            ticket.Precio = entrada.ObtienePrecio(Funcion, feriados);
            ticket.Venta = this;
            Tickets.Add(ticket);
        }
    }

    public class VentaDetalle : Entidad
    {
        public VentaDetalle(Entrada entrada, int cantidad)
        {
            Entrada = entrada;
            Cantidad = cantidad;
        }

        public int VentaId { get; set; }

        public int EntradaId { get; set; }

        public int Cantidad { get; set; }

        public decimal Precio { get; set; }

        public virtual Venta Venta { get; set; }

        public virtual Entrada Entrada { get; set; }

    }
}
