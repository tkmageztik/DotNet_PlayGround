﻿using System;

namespace CinePapayita.Dominio
{
    public class Precio : Entidad
    {
        public int EntradaId { get; set; }
        
        public virtual Entrada Entrada { get; set; }
        public decimal Valor { get; set; }

        public DayOfWeek Dia { get; set; }
    }
}