namespace CinePapayita.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using AccesoADatos;
    using Dominio;
    using System.Collections.Generic;
    internal sealed class Configuration : DbMigrationsConfiguration<CinePapayita.AccesoADatos.CarritoContexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CinePapayita.AccesoADatos.CarritoContexto context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //


            CrearTipoDeEntradas(context);
            CrearFuncionInicial(context);

        }

        private void CrearFuncionInicial(CarritoContexto bd)
        {
            if (bd.Funciones.Any()) return;

            var funcion = new Funcion()
            {
                Horario = new DateTime(2016, 09, 10, 16, 0, 0),
                Sala = new Sala { Nombre = "Inka", Capacidad = 100 },
                Pelicula = new Pelicula { Nombre = "Escuadron Suicida" }
            };

            var entradas = bd.Entradas.Where(x => x.EsVigente).ToList();
            entradas.ForEach(entrada => funcion.EntradasDisponibles.Add(entrada));

            bd.Funciones.Add(funcion);

        }

        private void CrearTipoDeEntradas(CarritoContexto bd)
        {
            if (bd.Entradas.Any()) return;
            var menordeEdad = new MenorDeEdad() { EsVigente = true, };
            menordeEdad.Precios.Add(new Precio() { Valor = 7, Dia = DayOfWeek.Monday, Entrada = menordeEdad });
            menordeEdad.Precios.Add(new Precio() { Valor = 5, Dia = DayOfWeek.Tuesday, Entrada = menordeEdad });
            menordeEdad.Precios.Add(new Precio() { Valor = 7, Dia = DayOfWeek.Wednesday, Entrada = menordeEdad });
            menordeEdad.Precios.Add(new Precio() { Valor = 7, Dia = DayOfWeek.Thursday, Entrada = menordeEdad });
            menordeEdad.Precios.Add(new Precio() { Valor = 7, Dia = DayOfWeek.Friday, Entrada = menordeEdad });
            menordeEdad.Precios.Add(new Precio() { Valor = 7, Dia = DayOfWeek.Saturday, Entrada = menordeEdad });
            menordeEdad.Precios.Add(new Precio() { Valor = 9, Dia = DayOfWeek.Sunday, Entrada = menordeEdad });

            var adulto = new Adulto() { EsVigente = true, };
            adulto.Precios.Add(new Precio() { Valor = 12, Dia = DayOfWeek.Monday, Entrada = adulto });
            adulto.Precios.Add(new Precio() { Valor = 10, Dia = DayOfWeek.Tuesday, Entrada = adulto });
            adulto.Precios.Add(new Precio() { Valor = 12, Dia = DayOfWeek.Wednesday, Entrada = adulto });
            adulto.Precios.Add(new Precio() { Valor = 12, Dia = DayOfWeek.Thursday, Entrada = adulto });
            adulto.Precios.Add(new Precio() { Valor = 12, Dia = DayOfWeek.Friday, Entrada = adulto });
            adulto.Precios.Add(new Precio() { Valor = 12, Dia = DayOfWeek.Saturday, Entrada = adulto });
            adulto.Precios.Add(new Precio() { Valor = 15, Dia = DayOfWeek.Sunday, Entrada = adulto });

            var adultoMayor = new AdultoMayor() { EsVigente = true, };
            adultoMayor.Precios.Add(new Precio() { Valor = 10, Dia = DayOfWeek.Monday, Entrada = adultoMayor });
            adultoMayor.Precios.Add(new Precio() { Valor = 9, Dia = DayOfWeek.Tuesday, Entrada = adultoMayor });
            adultoMayor.Precios.Add(new Precio() { Valor = 10, Dia = DayOfWeek.Wednesday, Entrada = adultoMayor });
            adultoMayor.Precios.Add(new Precio() { Valor = 10, Dia = DayOfWeek.Thursday, Entrada = adultoMayor });
            adultoMayor.Precios.Add(new Precio() { Valor = 10, Dia = DayOfWeek.Friday, Entrada = adultoMayor });
            adultoMayor.Precios.Add(new Precio() { Valor = 10, Dia = DayOfWeek.Saturday, Entrada = adultoMayor });
            adultoMayor.Precios.Add(new Precio() { Valor = 13, Dia = DayOfWeek.Sunday, Entrada = adultoMayor });

            bd.Entradas.Add(menordeEdad);
            bd.Entradas.Add(adulto);
            bd.Entradas.Add(adultoMayor);
            bd.SaveChanges();
        }
    }
}
