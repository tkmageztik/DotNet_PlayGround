namespace CinePapayita.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModeloInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Entrada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EsVigente = c.Boolean(nullable: false),
                        Tipo = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Funcion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Horario = c.DateTime(nullable: false),
                        PeliculaId = c.Int(nullable: false),
                        SalaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pelicula", t => t.PeliculaId, cascadeDelete: true)
                .ForeignKey("dbo.Sala", t => t.SalaId, cascadeDelete: true)
                .Index(t => t.PeliculaId)
                .Index(t => t.SalaId);
            
            CreateTable(
                "dbo.Pelicula",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sala",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Capacidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Precio",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EntradaId = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Dia = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entrada", t => t.EntradaId, cascadeDelete: true)
                .Index(t => t.EntradaId);
            
            CreateTable(
                "dbo.Feriados",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VentaDetalle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VentaId = c.Int(nullable: false),
                        EntradaId = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entrada", t => t.EntradaId, cascadeDelete: true)
                .ForeignKey("dbo.Venta", t => t.VentaId, cascadeDelete: true)
                .Index(t => t.VentaId)
                .Index(t => t.EntradaId);
            
            CreateTable(
                "dbo.Venta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreCliente = c.String(),
                        FuncionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Funcion", t => t.FuncionId, cascadeDelete: true)
                .Index(t => t.FuncionId);
            
            CreateTable(
                "dbo.FuncionEntrada",
                c => new
                    {
                        Funcion_Id = c.Int(nullable: false),
                        Entrada_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Funcion_Id, t.Entrada_Id })
                .ForeignKey("dbo.Funcion", t => t.Funcion_Id, cascadeDelete: true)
                .ForeignKey("dbo.Entrada", t => t.Entrada_Id, cascadeDelete: true)
                .Index(t => t.Funcion_Id)
                .Index(t => t.Entrada_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VentaDetalle", "VentaId", "dbo.Venta");
            DropForeignKey("dbo.Venta", "FuncionId", "dbo.Funcion");
            DropForeignKey("dbo.VentaDetalle", "EntradaId", "dbo.Entrada");
            DropForeignKey("dbo.Precio", "EntradaId", "dbo.Entrada");
            DropForeignKey("dbo.Funcion", "SalaId", "dbo.Sala");
            DropForeignKey("dbo.Funcion", "PeliculaId", "dbo.Pelicula");
            DropForeignKey("dbo.FuncionEntrada", "Entrada_Id", "dbo.Entrada");
            DropForeignKey("dbo.FuncionEntrada", "Funcion_Id", "dbo.Funcion");
            DropIndex("dbo.FuncionEntrada", new[] { "Entrada_Id" });
            DropIndex("dbo.FuncionEntrada", new[] { "Funcion_Id" });
            DropIndex("dbo.Venta", new[] { "FuncionId" });
            DropIndex("dbo.VentaDetalle", new[] { "EntradaId" });
            DropIndex("dbo.VentaDetalle", new[] { "VentaId" });
            DropIndex("dbo.Precio", new[] { "EntradaId" });
            DropIndex("dbo.Funcion", new[] { "SalaId" });
            DropIndex("dbo.Funcion", new[] { "PeliculaId" });
            DropTable("dbo.FuncionEntrada");
            DropTable("dbo.Venta");
            DropTable("dbo.VentaDetalle");
            DropTable("dbo.Feriados");
            DropTable("dbo.Precio");
            DropTable("dbo.Sala");
            DropTable("dbo.Pelicula");
            DropTable("dbo.Funcion");
            DropTable("dbo.Entrada");
        }
    }
}
