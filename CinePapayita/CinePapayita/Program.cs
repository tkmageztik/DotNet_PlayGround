﻿using CinePapayita.AccesoADatos;
using CinePapayita.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinePapayita
{
    class Program
    {
        static void Main(string[] args)
        {

            var bd = new CarritoContexto();
            var funcion = bd.Funciones.FirstOrDefault();
            var feriados = bd.Feriados.Where(x=> x.Fecha> new DateTime(2016,1,1)).ToList();

            var entrada = bd.Entradas.OfType<Adulto>().FirstOrDefault(x=>x.EsVigente);

            var venta = new Venta(funcion, "Uzi Mamani");

            venta.AgregaTicket(entrada, 2, feriados);

            bd.Ventas.Add(venta);
        }
    }
}
